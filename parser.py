from itertools import zip_longest
from datetime import datetime
from bs4 import BeautifulSoup
from icalendar import Calendar, Event


class PHCalParser:

	def __init__(self):
		self.event_name = ""
		self.event_list = []
		self.soup = None
		self.eventocc = None
		self.eventplace = None
		self.event_soup = None
		self.event_counter = 0
		self.cal = Calendar()

	def read_input(self, form_title, form_data):
		self.event_name = form_title
		self.soup = BeautifulSoup(form_data, "html.parser")
		self.eventocc = self.soup.select("p[class*=tx-phbern]")
		self.eventplace = self.soup.select("p[class*=bodytext]")

	def write_event_list(self):
		for (i, p) in zip_longest(self.eventocc, self.eventplace):
			ev_time_string = i.select_one('br').find_previous(string=True).replace("\t", "").replace("\r", "").replace("\n", "")
			ev_date = ev_time_string.split(", ")[1].split(" ")[0]
			ev_start_time = ev_time_string.split(", ")[1].split(" ")[1]
			ev_end_time = ev_time_string.split(" - ")[1].split(" ")[0]
			ev_date_components = ev_date.split(".")
			ev_start_components = ev_start_time.split(":")
			ev_end_components = ev_end_time.split(":")
			ev_datetime_start = datetime(int(ev_date_components[2]), int(ev_date_components[1]), int(ev_date_components[0]), int(ev_start_components[0]), int(ev_start_components[1]), 0)
			ev_datetime_end = datetime(int(ev_date_components[2]), int(ev_date_components[1]), int(ev_date_components[0]), int(ev_end_components[0]), int(ev_end_components[1]), 0)
			ev_place_string = p.text
			self.event_list.append({'start': ev_datetime_start, 'end': ev_datetime_end, 'place': ev_place_string})

	def write_cal(self):
		for i in self.event_list:
			event = Event()
			event.add('summary', self.event_name)
			event.add('description', self.event_name)
			event.add('location', i['place'])
			event.add('dtstart', i['start'])
			event.add('dtend', i['end'])
			self.cal.add_component(event)
			self.event_counter += 1

	def return_counter(self):
		return self.event_counter

	def return_cal(self):
		return self.cal.to_ical()
