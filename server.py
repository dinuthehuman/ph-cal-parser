#!/usr/bin/env python3

from flask import Flask, request, make_response
from cefpython3 import cefpython as cef
from parser import PHCalParser
import sys
import threading
import re


class PHCalServer:

	def __init__(self):
		self.PORT = 20001
		self.app = Flask(__name__)
		self.server_thread = threading.Thread(target=self.app.run, kwargs={"port": self.PORT})
		self.browser = None

	def force_alphanum(self, instr):
		instr = re.sub(r'\W+', '', instr)
		instr = instr.replace("Ä", "A").replace("ä", "a")
		instr = instr.replace("Ü", "U").replace("ü", "u")
		instr = instr.replace("Ö", "O").replace("ö", "o")
		return instr

	def serve_form(self):
		return self.app.send_static_file("form.html")

	def parse_data(self):
		parser = PHCalParser()
		parser.read_input(request.form['Description'], request.form['ModuleHTML'])
		parser.write_event_list()
		parser.write_cal()
		self.browser.ExecuteFunction("push_counter", parser.return_counter())
		response = make_response(parser.return_cal())
		response.headers["Content-Disposition"] = "attachment; filename=%s.ics" % self.force_alphanum(parser.event_name)
		return response

	def add_enpoints(self):
		self.app.add_url_rule('/', 'root', self.serve_form, methods=['GET'])
		self.app.add_url_rule('/api', 'api', self.parse_data, methods=['POST'])

	def start_server(self):
		self.add_enpoints()
		self.server_thread.start()

	def start_cef_app(self):
		sys.excepthook = cef.ExceptHook  # To shutdown all CEF processes on error
		cef.Initialize()
		self.browser = cef.CreateBrowserSync(url="http://localhost:%d/" % self.PORT, window_title="PH ical parser")
		cef.MessageLoop()
		cef.Shutdown()


if __name__ == "__main__":
	server = PHCalServer()
	server.start_server()
	server.start_cef_app()
